import { ObjectId } from "mongodb";
const LIBRARY = 'library';
const BRANCH_ID = 'branchId';

const resolvers = {
    Query: {

        libraries: async (_, __, context) => {
            const libraries = await context.dbClient.collection(LIBRARY).find({}).toArray();
            return libraries.map(library => {
                delete library.books;
                library.id = library._id;
                delete library._id;
                return library;
            });
        },
        books: async (_, __, context) => {
            const libraries = await context.dbClient.collection(LIBRARY).find({}).toArray();
            let books = [];

            for (const library of libraries) {
                const libraryBooks = library.books.map(book => {
                    book.branchId = library._id;
                    return book;
                });
                books = books.concat(libraryBooks);
            }

            return books;
        },
        getLibraryBooks: async(_, {libraryId}, context) => {
      
            const library = await context.dbClient.collection(LIBRARY).findOne({_id: new ObjectId(libraryId)});
            
            if(!library){
                throw Error(`Cannot find library with id ${libraryId}`);
            }

            return library.books.map(book => {
                book.branchId = library._id;
                return book;
            });
        },
        getLibrary: async(_, {id}, context) => {
            
            const library = await context.dbClient.collection(LIBRARY).findOne({_id: new ObjectId(id)});

            
            if(!library){
                throw Error(`Cannot find library with id ${id}`);
            }

            return {
                id: library._id,
                branch: library.branch,
                books: library.books.map(book => {
                    book[BRANCH_ID] = library._id;
                    return book;
                })
            };
        },

        authors: async (_, __, context) => {
            const libraries = await context.dbClient.collection(LIBRARY).find({}).toArray();
            const authors = new Set();

            libraries.forEach(library => {
                library.books.forEach(book => {
                    authors.add(
                        book.author
                    );
                });
            });

            const authorsArray = Array.from(authors);
            return authorsArray.map(author => ({name: author}));
        }
    },
    Mutation: {
        createBook: async (_, args, context) => {
            
            const { title, author, year, genre, branchId } = args.input;
            const newBook = {
                id: new ObjectId(),
                year: year,
                title: title,
                author: author.name,
                genre: genre ?? []
            };
            const response = await context.dbClient.collection(LIBRARY).updateOne({ _id: new ObjectId(branchId) }, { $push: { "books": newBook } }, { upsert: false });
            
            if (response.result.nModified !== 1) {
                throw Error('Error creating new book');
            }

            newBook.branchId = branchId;
            return newBook;
        },

        createLibrary: async (_, { branch }, context) => {

            const newLibrary = {
                _id: new ObjectId(),
                branch: branch,
                books: []
            };

            const response = await context.dbClient.collection(LIBRARY).insertOne(newLibrary);

            if (response.result.ok !== 1) {
                throw Error('Error creating new library');
            }
            newLibrary.id = newLibrary._id;
            delete newLibrary._id;

            return newLibrary;
        },

        deleteLibrary: async(_, {branchId}, context) => {
           
            const response = await context.dbClient.collection(LIBRARY).deleteOne({_id: new ObjectId(branchId)});
            
            if(!response.deletedCount || response.deletedCount === 0){
                throw Error(`Error deleting library with Id ${branchId}`);
            }

            return true;
        },

        deleteBook: async (_, {bookId, branchId}, context) => {
            
            const response = await context.dbClient.collection(LIBRARY).updateOne({ _id: new ObjectId(branchId) }, { $pull: { "books": {"id": new ObjectId(bookId)} } }, { upsert: false });
        
            if (response.result.nModified === 0) {
                throw Error(`Error deleting book with Id ${bookId}`);
            }
            
            return true;
        },


    },

    Book: {
        author: (parent) => {
            return {
                name: parent.author
            };
        }
    }
};

export default resolvers;