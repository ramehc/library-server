import {gql} from 'apollo-server-express';


const typeDefs = gql`
    type Library {
        id: String
        branch: String
        books: [Book]
    }

    type Book {
        id: String
        title: String
        author: Author
        year: Int
        genre: [String]
        branchId: String
    }

    input BookInput {
        title: String!
        author: AuthorInput!,
        branchId: String!
        year: Int!
        genre: [String]
    }

    type Author {
        name: String
    }

    input AuthorInput {
        name: String!
    }

    type Query {
        getLibraryBooks(libraryId: String): [Book]
        getLibrary(id: String): Library
        libraries: [Library]
        books: [Book]
        authors: [Author]
    }

    type Mutation{
        createLibrary(branch: String!): Library
        createBook(input: BookInput!): Book
        deleteLibrary(branchId: String!): Boolean
        deleteBook(bookId: String!, branchId: String!): Boolean
    }
`;


export default typeDefs;